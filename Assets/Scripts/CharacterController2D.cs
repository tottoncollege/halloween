﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController2D : MonoBehaviour
{
    InputActions input;
    Rigidbody2D rb;
    Collider2D col;

    public float jumpSpeed;
    public float MoveSpeed;
    float horizontal;

    public GameObject PlayerDeath;
    public GameObject Spider;
    public Sprite DeadPumpkin;

    private void Awake()
    {
        input = new InputActions();

        //set input actions to be called when event criteria met 
        //eg 'left' (A key) is performed, set horizontal variable to -1f
        input.Player.Left.performed += ctx => horizontal = -1f;
        input.Player.Right.performed += ctx => horizontal = 1f;
        //reset to 0f when input key is released
        input.Player.Left.canceled += ctx => horizontal = 0f;
        input.Player.Right.canceled += ctx => horizontal = 0f;
    }

    //Input enable and disable
    private void OnEnable()
    {
        input.Enable();
    }
    private void OnDisable()
    {
        input.Disable();
    }

    // Start is called before the first frame update
    void Start()
    {
        //Retrieve components attacked to this player GameObject
        rb = GetComponent<Rigidbody2D>();
        col = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //Turn isTrigger on if we are falling
        col.isTrigger = (rb.velocity.y > 0);

        //Movement
        this.transform.position += Vector3.right * horizontal * MoveSpeed * Time.deltaTime;

        //Clamp movement between set x positions
        this.transform.position = new Vector3(
            Mathf.Clamp(this.transform.position.x, -1.7f, 1.7f),
            this.transform.position.y, 
            this.transform.position.z);
        
        //Move Spider to player x pos
        Spider.transform.position = new Vector3(this.transform.position.x, Spider.transform.position.y, Spider.transform.position.z);
    }

    void Jump()
    {
        //add velocity to player in the x * set speed
        rb.velocity = Vector2.up * jumpSpeed;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Is it a platform we just hit?
        if(collision.gameObject.CompareTag("Platform"))
        {
            Debug.Log("Jump!");
            Jump(); //call our jump function above ^
            collision.gameObject.GetComponent<Rigidbody2D>().isKinematic = false;
            GameManager.AddPoint();
        }
        //Otherwise was it the First Platform? (We don't want to delete this one
        //as soon as the player starts...
        else if(collision.gameObject.CompareTag("FirstPlatform"))
        {
            Jump(); //call our jump function above ^
        }
    }
    //Collision Stay just incase we accidently land perfectly
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Platform"))
        {
            Debug.Log("Oops... Jump!");
            Jump();
            collision.gameObject.GetComponent<Rigidbody2D>().isKinematic = false;
        }
    }

    //Player death should follow behind the player so we don't fall for too long
    public void MovePlayerDeath()
    {
        //Set player death posisition to the position of our player - 7 units
        PlayerDeath.transform.position =  (this.transform.position.y - 7f) * Vector3.up;
    }

    //Called from the CollisionEvent script on the PlayerDeath GameObject. 
    public void KillPlayer ()
    {
        //Make Kinematic so we have full control of physics, stop all velocity, and switch the sprite
        rb.isKinematic = true;
        rb.velocity = Vector2.zero;
        GetComponent<SpriteRenderer>().sprite = DeadPumpkin;
    }

}
