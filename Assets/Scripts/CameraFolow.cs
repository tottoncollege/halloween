﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFolow : MonoBehaviour
{
    //Target will be our player pumpkin
    public Transform Target;

    // Update is called once per frame
    void Update()
    {
        //Follow the camera ONLY in the y. We don't want the camera to move left and right
        this.transform.position = Target.position.y * Vector3.up + Vector3.back * 10f;
    }
}
