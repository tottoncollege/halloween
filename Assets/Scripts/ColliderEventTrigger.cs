﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ColliderEventTrigger : MonoBehaviour
{

    public UnityEvent TargetFunctiontion;
    public string TagToTrigger = "Player";
    public bool ShouldDestroyObject = false;

    private void Start()
    {
        if (TargetFunctiontion == null)
            TargetFunctiontion = new UnityEvent();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(TagToTrigger))
            TriggerEvent(other.gameObject);
    }

    private void OnCollisionEnter (Collision other)
    {
        if (other.gameObject.CompareTag(TagToTrigger))
            TriggerEvent(other.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(TagToTrigger))
            TriggerEvent(other.gameObject);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag(TagToTrigger))
            TriggerEvent(other.gameObject);
    }

    void TriggerEvent (GameObject col)
    {
        if(ShouldDestroyObject)
            Destroy(col);
        TargetFunctiontion.Invoke();
    }
}
