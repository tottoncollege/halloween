﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [Header("GameObjects")]
    public GameObject Player;
    public GameObject Platform;
    public GameObject TopPlatform;
    public Text ScoreText;

    [Header("Settings")]
    public float distantBetweenPaltforms = 2f;

    static int Score = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Player.transform.position.y > TopPlatform.transform.position.y -distantBetweenPaltforms)
        {
            SpawnPlatform();
        }
        Time.timeScale += .01f * Time.deltaTime;
        ScoreText.text = Score.ToString();
    }

    void SpawnPlatform()
    {
        GameObject tmp = Instantiate(
            Platform, //GameObject to spawn
            new Vector3(Random.Range(-1.7f,1.7f), TopPlatform.transform.position.y + distantBetweenPaltforms), //Where?
            Quaternion.identity); //Rotation (none)

        TopPlatform = tmp;
        
    }

    public static void AddPoint()
    {
        Score++;
    }
}
